import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import AboutUs from './components/AboutUs/AboutUs';
import ContactUs from './components/ContactUs/ContactUs';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import Home from './components/Home/Home';
import Pricing from './components/Pricing/Pricing';
import styles from './scss/main.scss';

// import styles from './scss/fonts.scss';


ReactDOM.render(
    <div>
        <Router>
            <div>
                <Header/>
                <Switch>
                    <Route exact path="/">
                        <Home />
                    </Route>
                    <Route path="/about-us">
                        <AboutUs />
                    </Route>
                    <Route path="/pricing">
                        <Pricing />
                    </Route>
                    <Route path="/contact-us">
                        <ContactUs />
                    </Route>
                </Switch>
            </div>
        </Router>
        <Footer />
    </div>, document.getElementById('app')
);