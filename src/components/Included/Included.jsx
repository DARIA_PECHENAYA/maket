import React from 'react';
import styles from './Included.scss';

export default function Included() {

    return (
        <div className={styles.pricingCheckbox}>
            <div className={styles.pricingCheckboxItem}>
                <div className={styles.pricingCheckboxText}>
                    <img src='./assets/images/home/checkbox.png' />
                    <div>Maecenas faucibus mollis interdum</div>
                </div>
                <div className={styles.pricingCheckboxText}>
                    <img src='./assets/images/home/checkbox.png' />
                    <div>Cras justo odio, dapibus ac facilisis</div>
                </div>
                <div className={styles.pricingCheckboxText}>
                    <img src='./assets/images/home/checkbox.png' />
                    <div>Fringilla Mattis</div>
                </div>
            </div>

            <div className={styles.pricingCheckboxItem}>
                <div className={styles.pricingCheckboxText}>
                    <img src='./assets/images/home/checkbox.png' />
                    <div>Vestibulum id ligula porta felis euismod</div>
                </div>
                <div className={styles.pricingCheckboxText}>
                    <img src='./assets/images/home/checkbox.png' />
                    <div>Duis mollis, est non commodo luctus</div>
                </div>
                <div className={styles.pricingCheckboxText}>
                    <img src='./assets/images/home/checkbox.png' />
                    <div>Integer posuere</div>
                </div>
            </div>

            <div className={styles.pricingCheckboxItem}>
                <div className={styles.pricingCheckboxText}>
                    <img src='./assets/images/home/checkbox.png' />
                    <div>Nulla vitae elit libero, a pharetra augue</div>
                </div>
                <div className={styles.pricingCheckboxText}>
                    <img src='./assets/images/home/checkbox.png' />
                    <div>Curabitur blandit tempus porttitor</div>
                </div>
                <div className={styles.pricingCheckboxText}>
                    <img src='./assets/images/home/checkbox.png' />
                    <div>Nullam quis risus eget urna mollis</div>
                </div>
            </div>
        </div>
    )
}


