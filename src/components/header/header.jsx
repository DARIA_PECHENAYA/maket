import React from 'react';
import { useLocation } from 'react-router-dom'
import MenuItem from "../MenuItem/MenuItem";
import styles from './Header.scss';

const menuItems = [{
    route: "/",
    title: "Home"
}, {
    route: "/about-us",
    title: "About Us"
}, {
    route: "/pricing",
    title: "Pricing"
}, {
    route: "/contact-us",
    title: "Contact Us"
}]

export default function Header() {

    const location = useLocation();
    const pathName = location.pathname;

    return (
        <div className={styles.header}>
            <div className={styles.menuSection}>
                <div className={styles.menu}>
                    <div className={styles.wapik}>
                        <div>WAPIK</div>
                        <img src="./assets/images/header/line.png" />
                        <img src="./assets/images/header/phone.png" />
                        <div>+62 202 55 0117</div>
                    </div>
                    <div className={styles.menuClass}>
                        <div>
                            {
                                menuItems.map((menuItem, index) => {
                                    return <MenuItem key={index} route={menuItem.route} title={menuItem.title} active={pathName === menuItem.route} />
                                })
                            }
                        </div>
                        <div>
                            <button type="button">Get started</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}