import React from 'react';
import styles from './Home.scss';
import Plan from '../Plan/Plan';
import Included from '../Included/Included';
import Support from '../Support/Support';

export default function Home() {

    return (
        <div className={styles.home}>
            <div className={styles.welcome}>
                <div className={styles.welcomeItem}>
                    <div>
                        Welcome to Wapik
                    </div>
                    <div>
                        Wapik is multipurpose template, with modern and smart design. Wapik is perfect template for you!
                    </div>
                    <button>Learn more</button>
                </div>
            </div>
            <div className={styles.iconsSection}>
                <div className={styles.iconsItem}>
                    <div className={styles.iconsText}>
                        <div>Best of our features</div>
                        <div className={styles.line}></div>
                        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Integer posuere erat a ante venenatis dapibus posuere velit aliquet.
                        </div>
                    </div>
                    <div className={styles.ionicons}>
                        <div className={styles.ioniconsItem}>
                            <img src="./assets/images/home/ionicons.png" />
                            <div>Ionicons</div>
                            <div>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean eu leo quam. Pellentesque ornare sem.</div>
                        </div>
                        <div className={styles.ioniconsItem}>
                            <img src="./assets/images/home/creativeDesign.png" />
                            <div>Creative Design</div>
                            <div>Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</div>
                        </div>
                        <div className={styles.ioniconsItem}>
                            <img src="./assets/images/home/photoshopAndSketch.png" />
                            <div>Photoshop and Sketch</div>
                            <div>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas sed diam eget risus varius blandit sit amet non magna.</div>
                        </div>
                    </div>
                    <div>
                        <img src="./assets/images/home/line.png" />
                    </div>
                    <div className={styles.iconsWay}>
                        <div className={styles.iconsWayItem}>
                            <div className={styles.iconsIdia}>
                                <div className={styles.iconsIdiaItem}>
                                    <img src='./assets/images/home/browser.png' />
                                    <div>Cross Browser Compatibility</div>
                                </div>
                                <div>Donec ullamcorper nulla non metus auctor fringilla. Donec id elit non mi porta gravida at eget metus.</div>
                            </div>

                            <div className={styles.iconsIdia}>
                                <div className={styles.iconsIdiaItem}>
                                    <img src='./assets/images/home/creativeIdea.png' />
                                    <div>Creative Idea</div>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum id ligula porta felis euismod semper.</div>
                            </div>
                        </div>

                        <div className={styles.iconsWayItem}>
                            <div className={styles.iconsIdia}>
                                <div className={styles.iconsIdiaItem}>
                                    <img src='./assets/images/home/pixelPerfect.png' />
                                    <div>Pixel Perfect</div>
                                </div>
                                <div>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                            </div>

                            <div className={styles.iconsIdia}>
                                <div className={styles.iconsIdiaItem}>
                                    <img src='./assets/images/home/cloud.png' />
                                    <div>Free Forever and Ever</div>
                                </div>
                                <div>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec id elit non mi porta gravida at eget metus.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.phoneSection}>
                <div className={styles.phoneItem}>
                    <div className={styles.iconsText}>
                        <div>Looking for the perfect template to use?</div>
                        <div className={styles.line}></div>
                        <div>Donec id elit non mi porta gravida at eget metus.
                        </div>
                    </div>

                    <div className={styles.phoneScreen}>
                        <div className={styles.hand}>
                            <img src='./assets/images/home/hand.png' />
                        </div>
                        <div className={styles.phoneScreenItem}>
                            <div>Responsive Ready</div>
                            <div>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                                Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Aenean lacinia bibendum nulla sed consectetur.
                                Maecenas sed diam eget risus varius blandit sit amet non magna. Vestibulum id ligula porta felis euismod semper.
                            </div>
                            <div className={styles.phoneScreenItemText}>
                                <div>
                                    <img src='./assets/images/home/checkbox.png' />
                                </div>
                                <div>Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</div>
                            </div>
                            <div className={styles.phoneScreenItemText}>
                                <div>
                                    <img src='./assets/images/home/checkbox.png' />
                                </div>
                                <div>Donec id elit non mi porta gravida at eget metus.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.reviewSection}>
                <div className={styles.review}>
                    <div className={styles.reviewText}>
                        <div>This theme is beautiful, elegant and easy to customize.
                            I’ve been able to build an amazing site in just a minute.
                            Thank you very much for creating this impressive template!
                        </div>
                        <div>Valeria Boltneva</div>
                        <div>Director of Acme.Inc</div>
                    </div>
                    <div className={styles.reviewPicture}>
                        <img src='./assets/images/home/valeria.png' />
                    </div>
                </div>
            </div>
            <div className={styles.mapSection}>
                <div className={styles.map}>
                    <div className={styles.mapText}>
                        <div>
                            Built with High Attention to Details
                        </div>
                        <div>
                            Fusce dapibus, tellus ac cursus commodo, tortor mauris
                            condimentum nibh, ut fermentum massa justo sit amet risus.
                            Praesent commodo cursus magna, vel scelerisque nisl
                            consectetur et. Aenean lacinia bibendum nulla sed
                            consectetur. Maecenas sed diam eget risus varius
                            blandit sit amet non magna. Vestibulum id ligula
                            porta felis euismod semper. Integer posuere erat a
                            ante venenatis dapibus posuere velit aliquet. Donec
                            id elit non mi porta gravida at eget metus.
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.pricingSection}>
                <div className={styles.pricing}>
                    <div className={styles.iconsText}>
                        <div>Pricing & Plan</div>
                        <div className={styles.line}></div>
                        <div>Fusce dapibus, tellus ac cursus commodo, tortor mauris
                            condimentum nibh. <br/> Donec id elit non mi porta gravida
                            at eget metus.
                        </div>
                    </div>

                    <Plan />

                    <div className={styles.iconsText}>
                        <div>Included with all Plans</div>
                        <div className={styles.line}></div>
                    </div>
                    <Included />
                    <Support />
                </div>
            </div >
        </div >
    )
}