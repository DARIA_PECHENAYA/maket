import React from 'react';
import styles from './ContactUs.scss';
import { Form } from '../Form/Form';


export default function ContactUs() {

    return (
        <div className={styles.contactUs}>
            <div className={styles.map}>
            </div>

            <div className={styles.iconsSection}>
                <div className={styles.iconsItem}>
                    <div className={styles.iconsText}>
                        <div>Get In Touch With Us</div>
                        <div className={styles.line}></div>
                        <div> We are here to help. Want to learn more about our services?
                            Please get in touch, we'd love to hear from you!
                        </div>
                    </div>
                    <div className={styles.ionicons}>
                        <Form />
                        <div className={styles.headquarter}>
                            <div>Headquarter</div>
                            <div className={styles.headquarterItem}>
                                <div>
                                    <img src="./assets/images/contactUs/address.png" />
                                </div>
                                <div className={styles.headquarterText}>
                                    <div>610 Overlook Circle</div>
                                    <div>Suite 323</div>
                                    <div>Kalamazoo, MI 49009</div>
                                </div>
                            </div>
                            <div className={styles.headquarterItem}>
                                <div>
                                    <img src="./assets/images/contactUs/phone.png" />
                                </div>
                                <div className={styles.headquarterText}>
                                    <div>+62 202 555 0117</div>
                                </div>
                            </div>
                            <div className={styles.headquarterItem}>
                                <div>
                                    <img src="./assets/images/contactUs/mail.png" />
                                </div>
                                <div className={styles.headquarterText}>
                                    <div>hi@wapik.com</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}