import React from 'react';
import { Link } from "react-router-dom";

import styles from './MenuItem.scss';

export default function MenuItem(props) {
    return (
            <div className={`${props.active ? "active" : ""}`}>
                <Link to={props.route}>{props.title}</Link>
            </div>
        )
}