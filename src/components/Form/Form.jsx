import React from 'react';
import styles from './Form.scss';

export class Form extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            phone: '',
            message: ''
        };

        this.handleName = this.handleName.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handlePhone = this.handlePhone.bind(this);
        this.handleMessage = this.handleMessage.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleName(event) {
        this.setState({ name: event.target.value });
    }

    handleEmail(event) {
        this.setState({ email: event.target.value });
    }

    handlePhone(event) {
        this.setState({ phone: event.target.value });
    }

    handleMessage(event) {
        this.setState({ message: event.target.value });
    }

    handleSubmit(event) {
        alert('Спасибо,ваша заявка принята!Мы свяжемся с вами в ближайшее время.');
        event.preventDefault();
    }

    render() {
        return (
            <div className={styles.message}>
                <form className={styles.messageItem} onSubmit={this.handleSubmit}>
                    <input
                        name='name'
                        required={true}
                        type="name"
                        placeholder="name"
                        value={this.state.name}
                        onChange={this.handleName} />
                    <input
                        name='email'
                        required={true}
                        type="email"
                        placeholder="email"
                        value={this.state.email}
                        onChange={this.handleEmail} />
                    <input
                        name='phone'
                        required={true}
                        type="phone"
                        placeholder="phone"
                        value={this.state.phone}
                        onChange={this.handlePhone} />

                    <textarea
                        name='message'
                        required={true}
                        placeholder='Message'
                        value={this.state.message}
                        onChange={this.handleMessage} />

                    <input 
                        name='submit' 
                        type="submit" 
                        value="Send message" />
                </form>
            </div>
        );
    }
}