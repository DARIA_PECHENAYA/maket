import React from 'react';
import styles from './AboutUs.scss';

export default function AboutUs() {

    return (
        <div className={styles.aboutUs}>
            <div className={styles.welcome}>
                <div className={styles.welcomeItem}>
                    <div>
                        Cras justo odio, dapibus ac facilisis <br />in egestas eget quam
                    </div>
                    <button>Start Your Free Trial</button>
                </div>
            </div>

            <div className={styles.iconsSection}>
                <div className={styles.iconsItem}>
                    <div className={styles.iconsText}>
                        <div>About Us</div>
                        <div className={styles.line}></div>
                        <div>Duis mollis, est non commodo luctus, nisi erat porttitor ligula,
                            eget lacinia odio sem nec elit. Donec id elit non mi porta gravida
                            at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor
                            mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                            Nullam id dolor id nibh ultricies vehicula ut id elit. Vestibulum
                            id ligula porta felis euismod semper. Donec ullamcorper nulla non
                            metus auctor fringilla..
                        </div>
                    </div>
                    <div className={styles.ionicons}>
                        <div className={styles.ioniconsItem}>
                            <img src="./assets/images/home/ionicons.png" />
                            <div>Ionicons</div>
                            <div>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean eu leo quam. Pellentesque ornare sem.</div>
                        </div>
                        <div className={styles.ioniconsItem}>
                            <img src="./assets/images/home/creativeDesign.png" />
                            <div>Creative Design</div>
                            <div>Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</div>
                        </div>
                        <div className={styles.ioniconsItem}>
                            <img src="./assets/images/home/photoshopAndSketch.png" />
                            <div>Photoshop and Sketch</div>
                            <div>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas sed diam eget risus varius blandit sit amet non magna.</div>
                        </div>
                    </div>
                </div>
            </div>

            <div className={styles.teamSection}>
                <div className={styles.iconsItem}>
                    <div className={styles.iconsText}>
                        <div>Meet Our Team</div>
                        <div className={styles.line}></div>
                        <div>Curabitur blandit tempus porttitor. Duis mollis, est non commodo
                            luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
                            Cum sociis natoque penatibus et magnis dis parturient montes,
                            nascetur ridiculus mus.
                        </div>
                    </div>
                    <div className={styles.ionicons}>
                        <div className={styles.ioniconsItem}>
                            <img src="./assets/images/aboutUs/joyce.png" />
                            <div>Joyce Hudson</div>
                            <div>Founder & Designer</div>
                        </div>
                        <div className={styles.ioniconsItem}>
                            <img src="./assets/images/aboutUs/angela.png" />
                            <div>Angela Campbell</div>
                            <div>Project Manager</div>
                        </div>
                        <div className={styles.ioniconsItem}>
                            <img src="./assets/images/aboutUs/james.png" />
                            <div>James Walker</div>
                            <div>Developer</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}