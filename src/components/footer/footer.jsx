import React from 'react';
import styles from './Footer.scss';

export default function Footer() {
    return (
        <div className={styles.footer}>
            <div className={styles.downloadSection}>
                <div className={styles.downloadItem}>
                    <div className={styles.downloadText}>
                        <div>Do You Like Wapik? Download it Now</div>
                        <div>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</div>
                    </div>
                    <div>
                        <button>download now! - FREE forever</button>
                    </div>
                </div>
            </div>
            <div className={styles.footerContent}>
                <div className={styles.footerContentItem}>
                    <div className={styles.wapik}>
                        <div className={styles.wapikItem}>
                            <div>wapik</div>
                            <div>@: hi@wapik.com</div>
                            <div>p: +62 202 555 0117</div>
                            <div>a: 610 Overlook Circle Suite 323<br />
                                Kalamazoo, MI 49009
                            </div>
                        </div>

                        <div className={styles.wapikItem}>
                            <div>Company</div>
                            <div>Home</div>
                            <div>About Us</div>
                            <div>Pricing</div>
                            <div>Contact Us</div>
                        </div>

                        <div className={styles.wapikItem}>
                            <div>Others</div>
                            <div>Help & Support</div>
                            <div>Privacy Policy</div>
                            <div>Terms</div>
                            <div>Sitemap</div>
                        </div>

                        <div className={styles.wapikItem}>
                            <div>Newsletter</div>
                            <div>Subscribe to our newsletter and get all the cool news</div>
                            <form>
                                <input type="text" placeholder='Enter Email' />
                            </form>
                        </div>
                    </div>

                    <div className={styles.footerLine}></div>
                    <div className={styles.footerIcons}>
                        <div>All rights reserved 2015.</div>
                        <div className={styles.footerIconsItem}>
                            <div>
                                <img src='./assets/images/footer/twitter.png' />
                            </div>
                            <div>
                                <img src='./assets/images/footer/facebook.png' />
                            </div>
                            <div>
                                <img src='./assets/images/footer/instagram.png' />
                            </div>
                            <div>
                                <img src='./assets/images/footer/dribbble.png' />
                            </div>
                            <div>
                                <img src='./assets/images/footer/github.png' />
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}