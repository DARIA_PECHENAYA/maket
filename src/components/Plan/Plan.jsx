import React from 'react';
import styles from './Plan.scss';

export default function Plan() {

    return (
        
    <div className={styles.pricingSelect}>
        <div className={styles.pricingSelectItem}>
            <div>Basic</div>
            <div>$199</div>
            <div>/month</div>
            <div>Justo Fringilla</div>
            <div>Mollis Sit Nullam</div>
            <div>Ultricies Purus Amet</div>
            <div>Cras Inceptos</div>
            <div>
                <button>Select plan</button>
            </div>
        </div>

        <div className={styles.pricingSelectItem}>
            <div>Plus</div>
            <div>$399</div>
            <div>/month</div>
            <div>Justo Fringilla</div>
            <div>Mollis Sit Nullam</div>
            <div>Ultricies Purus Amet</div>
            <div>Cras Inceptos</div>
            <div>
                <button>Select plan</button>
            </div>
        </div>

        <div className={styles.pricingSelectItem}>
            <div>Premium</div>
            <div>$699</div>
            <div>/month</div>
            <div>Justo Fringilla</div>
            <div>Mollis Sit Nullam</div>
            <div>Ultricies Purus Amet</div>
            <div>Cras Inceptos</div>
            <div>
                <button>Select plan</button>
            </div>
        </div>
    </div>
    )
}