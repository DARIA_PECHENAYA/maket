import React from 'react';
import styles from './Pricing.scss';
import Plan from '../Plan/Plan';
import Included from '../Included/Included';
import Support from '../Support/Support';

export default function Pricing() {

    return (
        <div className={styles.pricing}>
            <div className={styles.plan}>
                <div className={styles.planItem}>
                    <div className={styles.iconsText}>
                        <div>Get Started with Wapik Today
                            All Plans include a 30-Day Trial Period</div>
                        <div className={styles.line}></div>
                        <div> Cras mattis consectetur purus sit amet fermentum.</div>
                    </div>
                    <Plan />
                    <div className={styles.iconsText}>
                        <div>Limited Time Offer. All plans are FREE 14 days!</div>
                    </div>
                </div>
            </div>

            <div className={styles.includedSection}>
                <div className={styles.iconsText}>
                    <div>Included With All Plans</div>
                    <div className={styles.line}></div>
                </div>
                <div><Included /></div>
                <div><Support /></div>
            </div>
        </div>
    )
}