import React from 'react';
import styles from './Support.scss';

export default function Support() {

    return (
        <div className={styles.supportSection}>
            <div >
                <img src='./assets/images/home/circle.png' />
            </div>
            <div className={styles.iconsText}>
                <div>Need Help?</div>
                <div className={styles.line}></div>
                <div>Contact our Customer Support that is always ready to help you with any possible
                    questions, problems or information.
                </div>
                <div className={styles.support}>
                    <a href='#'>support@wapik.com</a>
                </div>
            </div>

        </div>
    )
}